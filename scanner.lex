%{
#include "parser.h"
%}

%%

 /* Quoting all string in order to facilitate for
    emacs's C module :-) */
[ \t\n] ;			/* skip ws */
"if" { return IF; }
"elsif" { return ELSIF; }
"else" { return ELSE; }
"while" { return WHILE; }
"my" { return MY; }
"write_int" { return WRT_INT; }
"break" { return BREAK; }
"next" { return NEXT; }
"return" { return RETURN; }
"shift" { return SHFT; }
"==" { return EQL; }
"!=" { return NE; }
">=" { return GEQL; }
"<=" { return LEQL; }
">"  { return GT; }
"<"  { return LT; }
"||" { return OR; }
"&&" { return AND; }
"!"  { return NOT; }

[a-zA-Z_][a-zA-Z0-9_]* {
  yylval.fun.id = strdup(yytext);
  return IDENTIFIER;
}
[$][a-zA-Z_][a-zA-Z0-9_]* {
  yylval.var.id = strdup(yytext);
  return VARIABLE;
}
[0-9]+ {
  yylval.ival = atoi(yytext);
  return INTEGER;
}
  /*[#].* { } */
. { return yytext[0]; }

%%
