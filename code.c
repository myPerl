#include <stdio.h>
#include "code.h"
#include "vm.h"

#define SEG_SIZE 4096

int code_offset = 0;
code_t code[SEG_SIZE];

struct ops ops[] = {
  { "HALT", vm_halt },
  { "ADD", vm_add },
  { "SUB", vm_sub },
  { "MUL", vm_mul },
  { "DIV", vm_div },
  { "MOD", vm_mod },
  { "CMP_EQL", vm_cmp_eql },
  { "CMP_NE", vm_cmp_ne },
  { "CMP_GEQL", vm_cmp_geql },
  { "CMP_LEQL", vm_cmp_leql },
  { "CMP_GT", vm_cmp_gt },
  { "CMP_LT", vm_cmp_lt },
  { "CMP_OR", vm_cmp_or },
  { "CMP_AND", vm_cmp_and },
  { "CMP_NOT", vm_cmp_not },
  { "LOAD_VAR", vm_load_var },
  { "LOAD_INT", vm_load_int },
  { "LOAD_OFF", vm_load_off },
  { "STORE", vm_store },
  { "JMP_FALSE", vm_jmp_false },
  { "GOTO", vm_goto },
  { "WRITE_INT", vm_write_int },
  { "PUSH_INT", vm_push_int },
  { "PUSH_VAR", vm_push_var },
  { "CALL", vm_call },
  { "RET", vm_ret },
  { "SHIFT", vm_shift }
};

int
reserve_loc(void)
{
  return code_offset++;
}

int
gen_label(void)
{
  return code_offset;
}

void
gen_code(int op, int arg)
{ 
  code[code_offset].op = op;
  code[code_offset++].arg = arg;
}

void
back_patch(int addr, int op, int arg)
{
  code[addr].op = op;
  code[addr].arg = arg;
}

void
print_code(char *outfile)
{
  int i;
  FILE *fp;

  if (outfile) {
    fp = fopen(outfile, "w");
    if (! fp) {
      perror("fopen");
      fprintf(stderr, "Erro ao abrir %s\n", outfile);
    }
  } else {
    fp = stdout;
  }

  for (i = 0; i < code_offset; ++i) {
    fprintf(fp, "%d: %s %d\n", i, ops[code[i].op].opname, code[i].arg);
  }
}

