#ifndef __SYMTAB_H__
#define __SYMTAB_H__

typedef struct symtab {
  char *name;
  int offset;
  int scope;
  int addr;
  int dim;
  int size;
  struct symtab *next;
} symtab_t;

/* defined in symtab.c */
extern symtab_t *symtab[];

/* defined in parser.y */
extern int scope;

void initsym(void);
symtab_t *putsym(char *name, int scope, int addr, int dim, int size);
symtab_t *getsym(char *name, int scope);
void scoperem(int scope);

#endif	/* !__SYMTAB_H__ */
