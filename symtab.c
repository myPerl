#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "symtab.h"
#include "heap.h"
#include "code.h"

#define MAX_DEPTH 1024		/* maximum value of scope */

symtab_t *symtab[MAX_DEPTH];

void
initsym(void)
{
  memset(symtab, 0, MAX_DEPTH*sizeof(symtab_t));
}

symtab_t *
putsym(char *name, int scope, int addr, int dim, int size)
{
  symtab_t *node = calloc(1, sizeof(symtab_t));

  node->name = name;		/* the scanner strduped it. */
  node->offset = data_location(size);
  node->scope = scope;
  node->addr = addr;
  node->dim = dim;
  node->size = size;
  node->next = symtab[scope];
  symtab[scope] = node;		/* lazy user proof :-) */

  return node;
}

symtab_t *
getsym(char *name, int scope)
{
  symtab_t *p;
  int i;

  for (i = scope; i >= 0; --i)
    for (p = symtab[i]; p; p = p->next)
      if (strcmp(p->name, name) == 0)
	return p;

  return NULL;
}

static void
scoperem_aux(symtab_t *node)
{
  if (! node)
    return;

  scoperem_aux(node->next);
  free(node->name);
  free(node);
}

void
scoperem(int scope)
{
  scoperem_aux(symtab[scope]);
  symtab[scope] = NULL;
}
      
