CC = gcc
CFLAGS = -Wall -ggdb
LDADD = -ll -ly

LEX = flex
LFLAGS = 

YACC = bison
YFLAGS = -v -d

TARGET = myperl

SOURCES = parser.c parser.h \
          scanner.c \
          symtab.c symtab.h \
          heap.c heap.h \
          code.c code.h \
          vm.c vm.h

$(TARGET): $(SOURCES)
	$(CC) $(CFLAGS) $(filter %.c,$^) -o $@ $(LDADD)

parser.c: parser.y symtab.c symtab.h
	$(YACC) $(YFLAGS) $(filter %.y,$^) -o $@

parser.h: parser.c

scanner.c: scanner.lex parser.c parser.h
	$(LEX) $(LFLAGS) -o $@ $(filter %.lex,$^)

.PHONY: clean
clean:
	rm -f *.output *~ semantic.cache* *.o \
        parser.[ch] scanner.c $(TARGET)
	find . -type f -name *~ -exec rm -f {} \;

