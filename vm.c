#include <stdio.h>
#include <assert.h>
#include "heap.h"
#include "code.h"
#include "symtab.h"
#include "vm.h"

#define STACK_SIZE 4096

/* 
 * Pilha de execucao.
 *
 * Essa pilha eh a usada por praticamente tds as
 * rotinas da maquina virtual
 */
static int top = -1;
static int stack[STACK_SIZE];

/* 
 * Pilha usada para armazenar informacoes especificas
 * das chamadas de funcao
 */
static int fun_top = -1;
static int fun_stack[STACK_SIZE];

/* Program counter */
static int pc = 0;

/* Operacoes binarias */
enum {
  O_ADD, O_SUB, O_MUL, O_DIV, O_MOD,
  O_EQL, O_NE, O_GEQL, O_LEQL, O_GT, O_LT,
  O_OR, O_NOT, O_AND };

static void
bin_op(int op)
{
  assert(top >= 1);
  switch(op) {
  case O_ADD:
    stack[top-1] = stack[top-1] + stack[top];
    break;
  case O_SUB:
    stack[top-1] = stack[top-1] - stack[top];
    break;
  case O_MUL:
    stack[top-1] = stack[top-1] * stack[top];
    break;
  case O_DIV:
    stack[top-1] = stack[top-1] / stack[top];
    break;
  case O_MOD:
    stack[top-1] = stack[top-1] % stack[top];
    break;
  case O_EQL:
    stack[top-1] = stack[top-1] == stack[top];
    break;
  case O_NE:
    stack[top-1] = stack[top-1] != stack[top];
    break;
  case O_GEQL:
    stack[top-1] = stack[top-1] >= stack[top];
    break;
  case O_LEQL:
    stack[top-1] = stack[top-1] <= stack[top];
    break;
  case O_GT:
    stack[top-1] = stack[top-1] > stack[top];
    break;
  case O_LT:
    stack[top-1] = stack[top-1] < stack[top];
    break;
  case O_OR:
    stack[top-1] = stack[top-1] || stack[top];
    break;
  case O_AND:
    stack[top-1] = stack[top-1] && stack[top];
    break;
  case O_NOT:
    stack[top-1] = !stack[top-1];
    top++;			/* we do not reduce the stack */
    break;
  default:
    fprintf(stderr, "Operacao %d nao reconhecida\n", op);
    break;
  }
  --top;
  pc++;
}

void
vm_halt(int arg)
{
}

void
vm_add(int arg)
{
  bin_op(O_ADD);
}

void
vm_sub(int arg)
{
  bin_op(O_SUB);
}

void
vm_mul(int arg)
{
  bin_op(O_MUL);
}

void
vm_div(int arg)
{
  bin_op(O_DIV);
}

void
vm_mod(int arg)
{
  bin_op(O_MOD);
}

void
vm_cmp_eql(int arg)
{
  bin_op(O_EQL);
}

void
vm_cmp_ne(int arg)
{
  bin_op(O_NE);
}

void 
vm_cmp_geql(int arg)
{
  bin_op(O_GEQL);
}

void 
vm_cmp_leql(int arg)
{
  bin_op(O_LEQL);
}

void 
vm_cmp_gt(int arg)
{
  bin_op(O_GT);
}

void 
vm_cmp_lt(int arg)
{
  bin_op(O_LT);
}

void 
vm_cmp_or(int arg)
{
  bin_op(O_OR);
}

void 
vm_cmp_and(int arg)
{
  bin_op(O_AND);
}

void 
vm_cmp_not(int arg)
{
  bin_op(O_NOT);
}

void
vm_load_var(int arg)
{
  int off = stack[top--];
  stack[++top] = data_get(arg, off);
  pc++;
}

void
vm_load_int(int arg)
{
  stack[++top] = arg;
  pc++;
}

void
vm_load_off(int arg)
{
  stack[++top] = arg;
  pc++;
}

void
vm_store(int arg)
{
  int off = stack[top--];
  assert(top >= 0);
  data_put(arg, off, stack[top]);
  top--;
  pc++;
}

void
vm_jmp_false(int arg)
{
  if (stack[top] == 0)
    pc = arg;
  else
    pc++;
  top--;
}

void
vm_goto(int arg)
{
  pc = arg;
}

void
vm_write_int(int arg)
{
  printf("%d\n", stack[top]);
  top--;
  pc++;
}

void
vm_call(int arg)
{
  int nargs = fun_stack[fun_top-1];

  scope++;
  /* store the base pointer */
  fun_stack[++fun_top] = top-nargs+1;
  pc = arg;
}

void
vm_push_int(int arg)
{
  fun_stack[++fun_top] = arg;
  pc++;
}

void
vm_push_var(int arg)
{
  fun_stack[++fun_top] = data_get(arg, stack[top--]);
  pc++;
}

void
vm_ret(int arg)
{
  int nargs;

  fun_top--;			/* take off the base pointer */
  pc = fun_stack[fun_top--];  
  nargs = fun_stack[fun_top--];

  top -= nargs;

  assert(fun_top >= -1 && top >= -1);

  scoperem(scope--);
}

void
vm_shift(int arg)
{
  int bp = fun_stack[fun_top];
  stack[++top] = stack[bp];
  fun_stack[fun_top]++;
  pc++;
}

int
vm_loop(void)
{
  while (code[pc].op != HALT) {
    ops[code[pc].op].op(code[pc].arg);
  }

  return 0;
}
