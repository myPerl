#ifndef __CODE_H__
#define __CODE_H__

enum {
  HALT,
  ADD,
  SUB,
  MUL,
  DIV,
  MOD,
  CMP_EQL,
  CMP_NE,
  CMP_GEQL,
  CMP_LEQL,
  CMP_GT,
  CMP_LT,
  CMP_OR,
  CMP_AND,
  CMP_NOT,
  LOAD_VAR,
  LOAD_INT,
  LOAD_OFF,
  STORE,
  JMP_FALSE,
  GOTO,
  WRITE_INT,
  PUSH_INT,
  PUSH_VAR,
  CALL,
  RET,
  SHIFT
};

/* For use by the virtual machine mainly */
struct ops {
  char *opname;
  void (*op)(int arg);
};

typedef struct code {
  int op;
  int arg;
} code_t;

/* defined in code.c */
extern struct ops ops[];
extern int code_offset;
extern code_t code[];

int reserve_loc(void);
int gen_label(void);
void gen_code(int op, int arg);
void back_patch(int addr, int op, int arg);
void print_code(char *output);

#endif	/* !__CODE_H__ */
