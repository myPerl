fun1 {
    my ($a, $b);
    $a = 3;
    $b = 3 * $a;
    $a = $a + $b;
    return $a;
}

fun2 {
    my $a;
    $a = 3;
    return $a;
}

my $a;

$a = fun1() + fun2();
write_int($a);
