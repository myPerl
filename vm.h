#ifndef __VM_H__
#define __VM_H__

/* Operations */
void vm_halt(int);
void vm_add(int);
void vm_sub(int);
void vm_mul(int);
void vm_div(int);
void vm_mod(int);
void vm_cmp_eql(int);
void vm_cmp_ne(int);
void vm_cmp_geql(int);
void vm_cmp_leql(int);
void vm_cmp_gt(int);
void vm_cmp_lt(int);
void vm_cmp_or(int);
void vm_cmp_and(int);
void vm_cmp_not(int);
void vm_load_var(int);
void vm_load_int(int);
void vm_load_off(int);
void vm_store(int);
void vm_jmp_false(int);
void vm_goto(int);
void vm_write_int(int);
void vm_push_int(int);
void vm_push_var(int);
void vm_call(int);
void vm_ret(int);
void vm_shift(int);

int vm_loop(void);

#endif	/* !__VM_H__ */
