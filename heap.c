#define SEG_SIZE 4096

static int data[SEG_SIZE];
static int data_offset = 0;

int
data_location(int size)
{
  int bak = data_offset;
  data_offset += size;
  return bak;
}

int
data_get(int addr, int off)
{
  return data[addr+off-1];
}

void
data_put(int addr, int off, int val)
{
  data[addr+off-1] = val;
}

