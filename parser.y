%{
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <getopt.h>

#include "symtab.h"
#include "heap.h"
#include "code.h"
#include "vm.h"

#define YYDEBUG 1

void yyerror(char *);
int yylex(void);
int scope = 0;
int breaks[16];
int nexts[16];
int bpos = 0;
int npos = 0;
extern FILE *yyin;

void
install(char *name, int dim, int size)
{
  symtab_t *node;

  node = getsym(name, scope);
  if (! node || node->scope < scope) {
    node = putsym(name, scope, 0, dim, size);
  } else {
    printf("%s jah estah definido neste escopo!\n", name);
  }
}

void
install_fun(char *name, int addr)
{
  symtab_t *node;

  node = getsym(name, scope);
  if (! node || node->scope < scope) {
    node = putsym(name, scope, addr, 0, 0);
  } else {
    printf("%s jah estah definido neste escopo!\n", name);
  }
}

void
context_check(int op, char *name)
{
  symtab_t *id;
  
  id = getsym(name, scope);
  if (! id) {
    printf("Simbolo nao declarado: %s\n", name);
  } else {
    gen_code(op, id->offset);
  }
}

void
call_fun(char *name)
{
  symtab_t *id;
  
  id = getsym(name, scope);
  if (! id) {
    printf("Fuancao nao declarado: %s\n", name);
  } else {
    gen_code(CALL, id->addr);
  }
}

%}

%union {
  int ival;
  struct label {
    int for_goto;
    int for_jmp_false;
    int for_else;
  } label;
  struct var {
    char *id;
    int  dim;
    int  size;
    int  off;
  } var;
  struct fun {
    char *id;
    int nargs;
    int addr;
  } fun;
}

%start program

%token <ival> INTEGER
%token <fun> IDENTIFIER
%token <var> VARIABLE
%token <label> IF WHILE BREAK NEXT
%token ELSIF ELSE MY WRT_INT RETURN EQL 
%token NE GEQL LEQL GT LT SHFT OR AND NOT

%type <var> var_dec dim_dec variable pos
%type <fun> fun_args fun_args_

%right '='
%left EQL NE GEQL LEQL GT LT
%left '+' '-'
%left '*' '/' '%'
%left '(' ')'
%left "!"
%left AND OR

%%

program: statements { gen_code(HALT, 0); YYACCEPT; }
;

statements: statements statement
  |         statement
;

statement: declarations ';'
  |        variable '=' expression ';'      { gen_code(LOAD_OFF, $1.off);
                                              context_check(STORE, $1.id); }
  |        expression ';'
  |        WRT_INT '(' INTEGER ')' ';'      { gen_code(LOAD_INT, $3);
	                                      gen_code(WRITE_INT, 0); }
  |        WRT_INT '(' variable ')' ';'     { gen_code(LOAD_OFF, $3.off);
                                              context_check(LOAD_VAR, $3.id);
	                                      gen_code(WRITE_INT, 0); }
  |        IF '(' expression ')' { $1.for_jmp_false = reserve_loc(); }
           '{'                   { scope++; }
           statements '}'        { $1.for_else = reserve_loc();
                                   back_patch($1.for_jmp_false,
					      JMP_FALSE,
					      gen_label());
				   scoperem(scope--); }
           else                  { back_patch($1.for_else,
					      JMP_FALSE,
					      gen_label()); }
  |        WHILE                 { $1.for_goto = gen_label(); }
           '(' expression ')'    { $1.for_jmp_false = reserve_loc(); }
           '{'                   { scope++; }
           statements '}'        { scoperem(scope--);
                                   gen_code(GOTO, $1.for_goto);
			           back_patch($1.for_jmp_false,
					      JMP_FALSE,
					      gen_label());
				   while(npos) {
				     back_patch(nexts[--npos],
						GOTO,
						$1.for_goto);
				   }
				   while(bpos) {
				     back_patch(breaks[--bpos],
						GOTO,
						gen_label());
				   } }
  |        NEXT ';'              { nexts[npos++] = reserve_loc(); }
  |        BREAK ';'             { breaks[bpos++] = reserve_loc(); }
  |        IDENTIFIER            { $1.addr = reserve_loc();}
           '{'                   { scope++; }
           statements '}'        { scoperem(scope--);
	                           gen_code(RET, 0);
	                           back_patch($1.addr++,
					      GOTO,
					      gen_label());
	                           install_fun($1.id, $1.addr); }
;

else: ELSE '{' { scope++; }
      statements '}' { scoperem(scope--); }
  |   /* */
;

variable:     VARIABLE pos       { $$ = $1;
	                           $$.off = $2.off; }
;

pos: pos '[' INTEGER ']' { $1.off *= $3;
	                   $$ = $1; }
  |  /* empty */         { $$.off = 1; }

var_dec:      VARIABLE dim_dec { $$ = $1;
                                 $$.dim = $2.dim;
                                 $$.size = $2.size; }
;

dim_dec: dim_dec '[' INTEGER ']' { $1.dim++;
                                   $1.size *= $3;
                                   $$ = $1; }
  |      /* empty */ { $$.dim = 1; $$.size = 1; }

declarations: MY var_dec { install($2.id, $2.dim, $2.size); }
  |           MY '(' id_list ')'
;

id_list: id_list ',' var_dec { install($3.id, $3.dim, $3.size); }
  |      var_dec             { install($1.id, $1.dim, $1.size); }
;

fun_args_: fun_args { $$ = $1; }
  |        /* */    { $$.nargs = 0; }
;

fun_args: fun_args ',' INTEGER  { gen_code(LOAD_INT, $3);
                                  $$.nargs = $1.nargs + 1; }
  |       fun_args ',' variable { gen_code(LOAD_OFF, $3.off);
                                  context_check(LOAD_VAR, $3.id);
                                  $$.nargs = $1.nargs + 1; }
  |       INTEGER               { gen_code(LOAD_INT, $1); $$.nargs = 1; }
  |       VARIABLE              { gen_code(LOAD_OFF, $1.off);
                                  context_check(LOAD_VAR, $1.id); $$.nargs = 1; }
;

expression: expression '+' expression { gen_code(ADD, 0); }
  |         expression '-' expression { gen_code(SUB, 0); }
  |         expression '*' expression { gen_code(MUL, 0); }
  |         expression '/' expression { gen_code(DIV, 0); }
  |         expression '%' expression { gen_code(MOD, 0); }
  |         expression EQL expression { gen_code(CMP_EQL, 0); }
  |         expression NE  expression { gen_code(CMP_NE, 0); }
  |         expression GEQL expression { gen_code(CMP_GEQL, 0); }
  |         expression LEQL expression { gen_code(CMP_LEQL, 0); }
  |         expression GT  expression { gen_code(CMP_GT, 0); }
  |         expression LT  expression { gen_code(CMP_LT, 0); }
  |         '(' expression ')'
  |         expression OR  expression { gen_code(CMP_OR, 0); }
  |         expression AND expression { gen_code(CMP_AND, 0); }
  |         NOT expression            { gen_code(CMP_NOT, 0); }
  |         IDENTIFIER '(' fun_args_ ')' { gen_code(PUSH_INT, $3.nargs);
	                                   /* +1: after load_int, +2: after call */
	                                   gen_code(PUSH_INT, gen_label()+2);
                                           call_fun($1.id); }
  |         variable                  { gen_code(LOAD_OFF, $1.off);
                                        context_check(LOAD_VAR, $1.id); }
  |         INTEGER                   { gen_code(LOAD_INT, $1); }
  |         RETURN INTEGER            { gen_code(LOAD_INT, $2);
                                        gen_code(RET, 0); }
  |         RETURN variable           { gen_code(LOAD_OFF, $2.off);
                                        context_check(LOAD_VAR, $2.id);
                                        gen_code(RET, 0); }
  |         SHFT                      { gen_code(SHIFT, 0); }
;

%%

void
usage(char *pname)
{
  fprintf(stderr, "Uso: %s [argumentos] <programa>\n", pname);
  fprintf(stderr, "Onde argumentos pode ser:\n");
  fprintf(stderr, "\t-o: Especifica arquivo de saida\n");
  fprintf(stderr, "\t-d: Habilita saida de debugging do parser\n");
  fprintf(stderr, "\t-h: Imprime essa mensagem e finaliza a execucao\n");

  exit(0);
}

void
yyerror(char *err)
{
  fprintf(stderr, "%s\n", err);
}

int
main(int argc, char **argv)
{
  char *pname = argv[0];
  char *outfile = NULL;
  int opt, ret;

  while ( (opt = getopt(argc, argv, "o:dh")) != -1) {
    switch(opt) {
    case 'd':			/* enable parser debugging */
      yydebug = 1;
      break;
    case 'o':
      outfile = optarg;
      break;
    case 'h':			/* print usage */
      usage(pname);
      break;
    }
  }

  if (optind >= argc) {		/* input file missing */
    usage(pname);
  }

  yyin = fopen(argv[optind], "r");
  if (! yyin) {
    perror("fopen");
    fprintf(stderr, "Erro ao abrir %s\n", argv[optind]);
    exit(errno);
  }

  /* init symbol table */
  initsym();

  ret = yyparse();

  if (outfile)
    print_code(outfile);

  vm_loop();

  return ret;
}
