#ifndef __HEAP_H__
#define __HEAP_H__

int data_location(int size);
int data_get(int addr, int off);
int data_put(int addr, int off, int val);

#endif	/* !__STACK_H__ */
